//
//  MyLocationController.swift
//  lokator
//
//  Created by Private on 1/31/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit
import CoreLocation

class MyLocationController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var LocationLabel: UILabel!
    
    @IBOutlet var locationView: UIView!
    var locationManager = CLLocationManager()
    var location: String = ""
    
    @IBAction func pressRefresh(_ sender: Any) {
        refreshLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func refreshLocation() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
                if placemarks![0].locality != nil {
                    self.location += String(describing: placemarks![0].locality!) + ", "
                }
                if placemarks![0].administrativeArea != nil {
                    self.location += String(describing: placemarks![0].administrativeArea!) + " "
                }
                if placemarks![0].postalCode != nil {
                    self.location += String(describing: placemarks![0].postalCode!) + ", "
                }
                if placemarks![0].country != nil {
                    self.location += String(describing: placemarks![0].country!)
                }
                self.LocationLabel.text = self.location
                self.location = ""
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed")
    }
}


